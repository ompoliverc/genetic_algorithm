from collections import namedtuple
from random import choices, randint, randrange, random
from typing import List, Callable, Tuple, Optional
from functools import partial
from util.analysis import timer

import time

Genome = List[int]
Population = List[Genome]
Thing = namedtuple('Thing', ['name', 'value', 'weight'])

FitnessFunction = Callable[[Genome], int]
PopulateFunction = Callable[[], Population]
SelectionFunction = Callable[[Population, FitnessFunction], Tuple[Genome, Genome]]
CrossoverFunction = Callable[[Genome, Genome], Tuple[Genome, Genome]]
MutationFunction = Callable[[Genome, Genome], Genome]

def generate_genome(length: int) -> Genome:
    return choices([0, 1], k=length)

def generate_population(size: int, genome_length: int) -> Population:
    return [generate_genome(genome_length) for _ in range(size)]

def fitness(genome: Genome, things: [Thing], weight_limit: int):
    if len(genome) != len(things):
        raise ValueError("Genome and things must be of the same length")

    weight = 0
    value = 0

    for i, thing in enumerate(things):
        if genome[i] == 1:
            weight += thing.weight
            value += thing.value

            if weight > weight_limit:
                return 0
    return value

def selection_pair(population: Population, fitness_function: FitnessFunction) -> Population:
    return choices(
        population=population,
        weights=[fitness_function(gene) for gene in population],
        k=2
    )

def single_point_crossover(a: Genome, b: Genome) -> Tuple[Genome, Genome]:
    if len(a) != len(b):
        raise ValueError("Genome lengths must be the same")

    length = len(a)
    if length < 2:
        return a, b

    p = randint(1, length -1)
    return a[0:p] + b[p:], b[0:p] + a[p:]

def mutation(genome: Genome, num: int = 1, probability: int = 0.5 ) -> Genome:
    for _ in range(num):
        index = randrange(len(genome))
        genome[index] = genome[index] if random() > probability else abs(genome[index -1])
    return genome

def run_evolution(
    populate_function: PopulateFunction,
    fitness_function: FitnessFunction,
    fitness_limit: int,
    selection_function: SelectionFunction = selection_pair,
    crossover_function: CrossoverFunction = single_point_crossover,
    mutation_function: MutationFunction = mutation,
    generation_limit: int = 100
) -> Tuple[Population, int]:
    population = populate_function()

    for i in range(generation_limit):
        population= sorted(
            population,
            key=lambda genome: fitness_function(genome),
            reverse=True
        )

        if fitness_function(population[0]) >= fitness_limit:
            break

        next_generation = population[0:2]
        for j in range(int(len(population) / 2) - 1):
            parents = selection_function(population, fitness_function)
            offspring_a, offspring_b = crossover_function(parents[0], parents[1])
            offspring_a = mutation_function(offspring_a)
            offspring_b = mutation_function(offspring_b)
            next_generation += [offspring_a, offspring_b]

        population = next_generation

    population= sorted(
            population,
            key = lambda genome: fitness_function(genome),
            reverse=True
        )

    return population, i

def genome_to_things(genome: Genome, things: [Thing]) -> [Thing]:
    result = []
    for i, thing in enumerate(things):
        if genome[i] == 1:
            result += [thing.name]
    return result

things = [
    Thing('Laptop', 500, 2200),
    Thing('Headphones', 150, 160),
    Thing('Coffee Mug', 60, 350),
    Thing('Notepad', 40, 333),
    Thing('Waterbottle', 30, 192),
]
def generate_things(num: int) -> [Thing]:
    return [Thing(f"thing{i}", i, i) for i in range(1, num+1)]

with timer():
    population, generations = run_evolution(
        populate_function=partial(generate_population, size=10, genome_length=len(things)),
        fitness_function=partial(fitness, things=things, weight_limit=3000),
        fitness_limit=740,
        generation_limit=100
    )
    print(f"number of generations: {generations}")
    print(f"best solution: {genome_to_things(population[0], things)}")