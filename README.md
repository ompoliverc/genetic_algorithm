# Genetic Algorithm

Built with Python3
[Thanks to Kie Codes For The Tutorial](https://www.youtube.com/channel/UC1cSiiIAx7Kni1ivz7-3rpQ)
## How To Run

`python genetic_algorithm/main.py`


## Todo

- [x] Create util for printing timing
- [ ] Seperate example implementation from genetic algorithm
- [ ] Remove need for fixed fitness limit (create a continous algorithm)
- [ ] GUI/Website